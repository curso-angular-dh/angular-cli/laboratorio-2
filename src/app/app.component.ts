import {Component, OnInit} from '@angular/core';
import {CardService} from './card/card.service';
import {Card} from './card/card';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public cards: Card[];

  constructor(private _cardService: CardService) {
  }

  ngOnInit(): void {
    this.cards = this._cardService.cards;
  }

}
