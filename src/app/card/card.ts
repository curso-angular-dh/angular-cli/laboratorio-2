export class Card {

  private _title: string;
  private _content: string;
  private _footer: string;


  constructor(title: string, content: string, footer: string) {
    this._title = title;
    this._content = content;
    this._footer = footer;
  }

  get title(): string {
    return this._title;
  }

  set title(title: string) {
    this._title = title;
  }

  get content(): string {
    return this._content;
  }

  set content(content: string) {
    this._content = content;
  }

  get footer(): string {
    return this._footer;
  }

  set footer(footer: string) {
    this._footer = footer;
  }
}
