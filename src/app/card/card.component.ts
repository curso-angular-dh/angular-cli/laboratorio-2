import {Component, Input, OnInit} from '@angular/core';
import {Card} from './card';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() card: Card;

  private readonly EMPTY = '';

  constructor() {
    this.card = new Card(this.EMPTY, 'Loading...', this.EMPTY);
  }

  ngOnInit() {
    console.log(this.card);
  }

}
