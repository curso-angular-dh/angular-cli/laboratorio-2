import {Injectable} from '@angular/core';
import {Card} from './card';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  private _cards: Card[];

  constructor() {
    this._cards = [];

    this._fillCards();
  }

  private _fillCards(): void {
    const content = 'Lorem ipsum dolor sit amet consectetur adipiscing elit ligula tristique quisque vivamus, nisi orci ultricies' +
      ' imperdiet donec aliquet vulputate dignissim et potenti, mauris neque eu cum purus pellentesque nam dis habitant egestas.' +
      ' Risus bibendum libero ut tempor cubilia vel magnis varius scelerisque montes aenean, convallis suspendisse curabitur taciti' +
      ' rutrum lectus vitae mattis at velit, platea vivamus proin commodo class nullam nunc eleifend venenatis sociosqu. Turpis';

    for (let i = 0; i < 10; i++) {
      this._cards.push(new Card(`Title ${i + 1}`, content, `Footer ${i + 1}`));
    }

    this._cards.push();
  }

  public get cards(): Card[] {
    return this._cards;
  }

  public set cards(cards: Card[]) {
    this._cards = cards;
  }
}
